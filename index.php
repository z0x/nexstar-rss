<?php

$site="wkrn.com";
$last_modified_file='last_modified';

$last_modified = file_get_contents($last_modified_file) ;

$wget_last_modified_cmd = "wget -Nnv --spider $site";
$wget_last_modified_arr = explode(" ", `$wget_last_modified_cmd`  );
#$wget_last_modified = "$wget_last_modified_arr[0] $wget_last_modified_arr[1]";


/*if ($wget_last_modified !== $last_modified) {
    file_put_contents($last_modified_file, $wget_last_modified);

}*/

$category_arr = [] ;
$grep_arr = ['news', 'top-news',
    'special-reports', 'national', 'sports',
];

foreach ($grep_arr as $category){
    $category_arr[$category] = [] ;
}


$grep_glue = '\\|';

$grep_str = '\'';
$grep_str .= implode($grep_glue, $grep_arr);
$grep_str .= '\'';

$dump_cmd = '\\lynx -dump -listonly -nonumbers -unique_urls  -hiddenlinks=ignore ';
$dump_cmd .= $site;
$dump_cmd .=' | grep ' . $grep_str ;

$relevant_links = [] ;
$links = array_unique( explode( "\n", `$dump_cmd` ) );

foreach ($links as $link){
    $split_url = explode('/', $link);
    if (is_numeric( end( $split_url))){
        array_push($relevant_links, $link);
    }
}

foreach ($relevant_links as $link){
    $split_url = explode('/', $link);
    $category = $split_url[3];
    echo $category;

    array_push($category_arr[$category], $link);
}



var_dump($category_arr);