<?php
namespace z0x\nexstar_rss;
class NexstarRss extends BuildRSS
{
    public $cache;
    function __construct($cache = NULL)
    {
        parent::__construct();
        $this->cache= new FeedCache();

    }

    function give_it_a_go_i_suppose(){
        if (isset($this->cache)){
            $this->cache->load();
        }
        $this->check();
        if ($this->links){
           $this->linkinfo();
           $this->cache->save($this->linkinfo, $this->cache->cache_file);
        }
    }
}