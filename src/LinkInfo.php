<?php

namespace z0x\nexstar_rss;

Class LinkInfo extends CategorizeFeeds
{
    private $grabber;
    private $headline_xpath="/html/head/title";
    private $byline_xpath="/html/body/div/div/div[2]/section/div[2]/div[1]/div/div[1]/div/div/div/div/div/div[1]/div/div/section/header/div/div[1]/ul/li/a";
    private $pubdate_xpath="/html/body/div/div/div[2]/section/div[2]/div[1]/div/div[1]/div/div/div/div/div/div[1]/div/div/section/header/div/div[1]/div/p[1]";
    private $article_textnode_video_xpath="//*[@id=\"responsive-wrapper anvato-player\"]";
    private $article_xpath="/html/body/div/div/div[2]/section/div[2]/div[1]/div/div[1]/div/div/div/div/div/div[1]/div/div/section/div/p";

    public $linkinfo;
    function __construct()
    {
        parent::__construct();
        $this->grabber = new Grabber();
    }

     public function linkinfo(){
        $this->get_article_info();
        $this->generate_categories();
        $this->linkinfo = [
            "site" => $this->site,
            "links" => $this->links,
            "links_by_category" => $this->links_by_category
        ];
    }

    private function get_article_info(){
        $count=0;
        foreach ($this->links as $link_info){
            $link =  $link_info['link'];
            echo "$count) getting $link...\n";
            $article_html = $this->grabber->grab($link);
            $article_arr =  $this->extract_article_nodes($article_html);

            $this->links[$count] = $this->links[$count] + $article_arr;
            $count++;
            }
    }
    
    private function extract_article_nodes($str){
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->loadHTML($str);
        $xpath = new \DOMXPath($dom);
        $article_html = "";
        $headline = $xpath->query($this->headline_xpath)->item(0)->textContent;

        $byline_xpath_object_size = $xpath->query($this->byline_xpath)->length;
            if ($byline_xpath_object_size > 0){
                $byline = $xpath->query($this->byline_xpath)->item(0)->textContent;
            }else {
                $byline = $this->site;
            }

        $pubdate_xpath_text = $xpath->query($this->pubdate_xpath)->item(0)->textContent;
        $pubDate_arr = explode(" ",trim($pubdate_xpath_text));
        $pubDate = "$pubDate_arr[1] $pubDate_arr[2] $pubDate_arr[3]";
        $article_xpath = $xpath->query($this->article_xpath);
        $article_node_count = $article_xpath->length;

        /*special cases. needs to be spun off*/
        if(!$article_node_count){
            $article_node_count= $xpath->query($this->article_textnode_video_xpath)->length;
            if(!$article_node_count){
                $headline .= "[???????????]";
            }else{
                $headline .= " [VIDEO]";
                $article_text_node = $xpath->query($this->article_textnode_video_xpath);
                $article_html = htmlentities($article_text_node->C14N());
            }
        /*end special cases*/

        }else {
            foreach ($xpath->query("$this->article_xpath") as $value){
                $article_html .= $value->C14N();
            }
        }

        $article_arr = [
            "title" => $headline,
            "author" => $byline,
            "pubDate" => $pubDate,
            "description" => $article_html,
        ];

        return $article_arr;
    }
}