<?php

namespace z0x\nexstar_rss;

class Grabber{
    function __construct()
    {
    }

    public function grab($url, $fallback = FALSE){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER,1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if($fallback){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //ignore cert errors.
        }
        $article=curl_exec($ch);
        if (! curl_errno($ch)){
            return $article;
        }else if(curl_errno($ch) === 35) {
            echo "SSL handshake failed. Attempting to ignore.\n";
            $this->grab($url, TRUE);
        } else {
            $this->grab($url);
        }
    }

}