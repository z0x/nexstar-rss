<?php

namespace z0x\nexstar_rss;

class FeedCache {

    
    private  $linkinfo;
    public $cache_file;
    public function __construct()
    {

    }

    public function save(array $linkinfo, string $cache_file){
        $this->linkinfo = (object) $linkinfo;
        $json = $this->mk_json();
        file_put_contents($cache_file ,$json);
    }
    
    protected function mk_json(){
        return json_encode([ "site" => $this->linkinfo->site,
                            "links " => $this->linkinfo->links,
                            "links_by_category" => $this->linkinfo->links_by_category]);
    }   
    
    public function load(){
        if (file_exists($this->cache_file)) {
            $nexstar_rss= new NexstarRss();
            return json_decode(file_get_contents($this->cache_file), true)[0];
            /*$ilinks= $json->links;
            $this->links_by_category = $json->links_by_category;*/
        }
    }


}