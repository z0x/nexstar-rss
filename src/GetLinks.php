<?php
namespace z0x\nexstar_rss;

class GetLinks 
{
    public $site;
    public $last_modified_file;
    public $grep_arr;
    public $links = [];


    private $cache ;

    function __construct()
    {
        $this->grep_arr = ['news', 'top-news',
                                'special-reports', 'national',
                                'sports',
                                ];
        $this->cache = new FeedCache();
        
    }

    public function  check(){
        //var_dump($this);
        $last_modified = trim(file_get_contents($this->last_modified_file));
        $curl_last_modified = trim($this->get_last_modified());

        if ($last_modified !== $curl_last_modified){
            file_put_contents($this->last_modified_file, $curl_last_modified);
            $this->get_links();
            return true;
        } else return false;

    }

    protected function get_last_modified(){
        if (!isset($this->last_modified_file)){

        }
        $curl_last_modified =explode("\n",ltrim(substr(`curl -I -L -k $this->site | grep Last-Modified `, 15)))[0]; //we're getting mutiple headers of the same thing.

        return $curl_last_modified;
    }


    protected function get_links(){
        $relevant_links = [] ;
        $io_init= array(
            0 => array("pipe", "r"),  // stdin
            1 => array("pipe", "w"),  // stdout
            2 => array("pipe", "w"),  // stderr
        );
        $grep_glue = '\\|';

        $grep_str = '\'';
        $grep_str .= implode($grep_glue, $this->grep_arr);
        $grep_str .= '\'';

        $dump_cmd = '\\lynx -dump -listonly -nonumbers -unique_urls  -hiddenlinks=ignore ';
        $dump_cmd .= $this->site;
        $dump_cmd .=' | grep ' . $grep_str ;

        $process = proc_open($dump_cmd, $io_init, $pipes, null, null);
        $raw_links = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);

        //$raw_links = exec($dump_cmd,$output, $return_code);

        //echo "lynx returned $return_code\n";
        //echo "output:";
        //var_dump($output);
        //var_dump($stderr);
        echo "raw links: $raw_links";
        //try curl if lynx fails
        if(empty($raw_links) AND strpos($stderr,"Alert!: Unable to make secure connection to remote host.") !== FALSE){
            echo "SSL connection failed. Attempting curl fallback\n";
            $grabber = new Grabber();
            $grabber->grab($this->site);
            $dump_cmd = '\\lynx -dump -listonly -nonumbers -unique_urls  -hiddenlinks=ignore ';
            $dump_cmd .= "tmp.html";
            $dump_cmd .=' | grep ' . $grep_str ;
            $raw_links = `$dump_cmd`;
        }

        $this->links = array_unique( explode( "\n", $raw_links ) );

        foreach ($this->links as $link){
            $split_url = explode('/', $link);
            if (is_numeric( end( $split_url))){
                if (substr($link, 0, strlen("file")) === "file") {
                    $link = substr($link, strlen("file://"));
                    $link  = "http://$this->site$link";
                }
                array_push($relevant_links, ["link" => $link] );
            }
        }
        $this->links=[];
        $this->links = $relevant_links;
        //this->links['link'] = $relevant_links;

        //$this->links=array_diff($relevant_links, $this->links);
    }

    private function curl_get(){

}

    public function save_obj_json($obj){
        return file_put_contents($this->last_modified_file, json_encode($obj));
    }
    public function load_obj_json(){
        return json_decode(file_get_contents($this->last_modified_file));
    }

}
